/**
 * \file
 *         Header file for the LSCHC implementation for IPv6 UDP headers
 *         (draft-ietf-lpwan-ipv6-static-context-hc-02)
 * \author Khaled Q. Abdelfadeel <khaled.abdelfadeel@mycit.ie>
 *
 */

#ifndef LSCHC_H_
#define LSCHC_H_

#include "lib/cbor.h"

#define LSCHC_NUM_IPV6_FIELDS		10
#define LSCHC_NUM_IPV6UDP_FIELDS	13

#define LSCHC_MAX_ALC_CoAP           0
#define LSCHC_MAX_TLC_UDP            1
#define LSCHC_MAX_TLC_ICMP           0
#define LSCHC_MAX_NLC_IP             2

#define LSCHC_NO_RULE 				0x1F

#define LSCHC_NO_NLC				0x03
#define LSCHC_NLC_R0				0x00
#define LSCHC_NLC_R1				0x01
#define LSCHC_NLC_R2				0x02

#define LSCHC_NO_TLC				0x0C
#define LSCHC_TLC_R0				0x00
#define LSCHC_TLC_R1				0x04
#define LSCHC_TLC_R2				0x08

#define LSCHC_NO_ALC				0x10
#define LSCHC_ALC_R0				0x00

#define LSCHC_FP_1                  1
#define LSCHC_FP_2                  2
#define LSCHC_FP_3                  3

#define LSCHC_NUM_IPV6_PREFIX       8
#define LSCHC_NUM_IPV6_IID			8

////////////  IPv6 ////////////////

#define LSCHC_IPv6Version_TV		3
#define LSCHC_IPv6Version_MO		5
#define LSCHC_IPv6Version_CDF		5

#define LSCHC_IPv6DiffServ_TV		3
#define LSCHC_IPv6DiffServ_MO		5
#define LSCHC_IPv6DiffServ_CDF		5

#define LSCHC_IPv6FlowLabel_TV		6
#define LSCHC_IPv6FlowLabel_MO		5
#define LSCHC_IPv6FlowLabel_CDF		5

#define LSCHC_IPv6Length_TV			5
#define LSCHC_IPv6Length_MO			5
#define LSCHC_IPv6Length_CDF		5

#define LSCHC_IPv6NextHeader_TV		8
#define LSCHC_IPv6NextHeader_MO		5
#define LSCHC_IPv6NextHeader_CDF	5

#define LSCHC_IPv6HopLimit_TV		15
#define LSCHC_IPv6HopLimit_MO		5
#define LSCHC_IPv6HopLimit_CDF		5

#define LSCHC_IPv6ESprefix_TV		40
#define LSCHC_IPv6ESprefix_MO		5
#define LSCHC_IPv6ESprefix_CDF		5

#define LSCHC_IPv6ESiid_TV			17
#define LSCHC_IPv6ESiid_MO			5
#define LSCHC_IPv6ESiid_CDF			5

#define LSCHC_IPv6LAprefix_TV		40
#define LSCHC_IPv6LAprefix_MO		5
#define LSCHC_IPv6LAprefix_CDF		5

#define LSCHC_IPv6LAiid_TV			17
#define LSCHC_IPv6LAiid_MO			5
#define LSCHC_IPv6LAiid_CDF			5

////////////  UDP ////////////////

#define LSCHC_UDPESport_TV			5
#define LSCHC_UDPESport_MO			5
#define LSCHC_UDPESport_CDF			5

#define LSCHC_UDPLAport_TV			5
#define LSCHC_UDPLAport_MO			5
#define LSCHC_UDPLAport_CDF			5

#define LSCHC_UDPLength_TV			5
#define LSCHC_UDPLength_MO			5
#define LSCHC_UDPLength_CDF			5

#define LSCHC_UDPChecksum_TV		5
#define LSCHC_UDPChecksum_MO		5
#define LSCHC_UDPChecksum_CDF		5

/* Default Target Values within the LSCHC */
#define LSCHC_DEF_IPV6_VERSION 			0x6
#define LSCHC_DEF_IPV6_DIFFSERV			0
#define LSCHC_DEF_IPV6_FLOWLABEL		0

#define LSCHC_IPV6_UDP_NEXTHEADER 		0x11
#define LSCHC_IPV6_ICMP_NEXTHEADER 		0x3a
#define LSCHC_IPV6_HOPOPT_NEXTHEADER 	0x0

#define LSCHC_DEF_IPV6_HOPLIMIT_64		0x40
#define LSCHC_DEF_IPV6_HOPLIMIT_63		0x3F
#define LSCHC_DEF_IPV6_HOPLIMIT_62		0x3E
#define LSCHC_DEF_IPV6_HOPLIMIT_61		0x3D
#define LSCHC_DEF_IPV6_HOPLIMIT_60		0x3C
#define LSCHC_DEF_IPV6_HOPLIMIT_59		0x3B


typedef enum{
	IPv6Version,
	IPv6DiffServ,
	IPv6FlowLabel,
	IPv6Length,
	IPv6NextHeader,
	IPv6HopLimit,
	IPv6ESprefix,
	IPv6ESiid,
	IPv6LCprefix,
	IPv6LAiid,
	UDPESport,
	UDPLAport,
	UDPLength,
	UDPChecksum,
	ICMPType,
	ICMPCode,
	ICMPChecksum
}LSCHC_FieldName;

typedef enum{
	Up,
	Dw,
	Bi
}LSCHC_DirectionIndicator;

typedef enum{
	Equal,		  // Equal
	Ignore,		  // Ignore
	MSB,		  // Most Significant Bit - MSB int
	MatchMapping  // Match Mapping
}LSCHC_MatchingOperator;

typedef enum{
	NoTSend,	  // Not Send
	ValueSent,    // Value Sent
	MappingSent,  // Mapping Sent
	CompL,	      // Compute Length
	LSB,		  // Least Significant Bit - LSB int
	ESiidDID,	  // Reconstruct ES IID from L2 header
	LAiidDID,     // Reconstruct the LA IID from L2 header
	CompChk       // Compute Checksum
}LSCHC_CDF;


////////            IP            //////////////////
typedef struct {
	LSCHC_FieldName field_name;
	char field_position;
	LSCHC_DirectionIndicator direction_indicator;
	unsigned char tv_data[LSCHC_IPv6Version_TV];
	cbor_stream_t tv_stream;
	unsigned char mo_data[LSCHC_IPv6Version_MO];
	cbor_stream_t mo_stream;
	unsigned char cdf_data[LSCHC_IPv6Version_CDF];
	cbor_stream_t cdf_stream;
}LSCHC_IPv6Version;

typedef struct {
	LSCHC_FieldName field_name;
	char field_position;
	LSCHC_DirectionIndicator direction_indicator;
	unsigned char tv_data[LSCHC_IPv6DiffServ_TV];
	cbor_stream_t tv_stream;
	unsigned char mo_data[LSCHC_IPv6DiffServ_MO];
	cbor_stream_t mo_stream;
	unsigned char cdf_data[LSCHC_IPv6DiffServ_CDF];
	cbor_stream_t cdf_stream;
}LSCHC_IPv6DiffServ;

typedef struct {
	LSCHC_FieldName field_name;
	char field_position;
	LSCHC_DirectionIndicator direction_indicator;
	unsigned char tv_data[LSCHC_IPv6FlowLabel_TV];
	cbor_stream_t tv_stream;
	unsigned char mo_data[LSCHC_IPv6FlowLabel_MO];
	cbor_stream_t mo_stream;
	unsigned char cdf_data[LSCHC_IPv6FlowLabel_CDF];
	cbor_stream_t cdf_stream;
}LSCHC_IPv6FlowLabel;

typedef struct {
	LSCHC_FieldName field_name;
	char field_position;
	LSCHC_DirectionIndicator direction_indicator;
	unsigned char tv_data[LSCHC_IPv6Length_TV];
	cbor_stream_t tv_stream;
	unsigned char mo_data[LSCHC_IPv6Length_MO];
	cbor_stream_t mo_stream;
	unsigned char cdf_data[LSCHC_IPv6Length_CDF];
	cbor_stream_t cdf_stream;
}LSCHC_IPv6Length;

typedef struct {
	LSCHC_FieldName field_name;
	char field_position;
	LSCHC_DirectionIndicator direction_indicator;
	unsigned char tv_data[LSCHC_IPv6NextHeader_TV];
	cbor_stream_t tv_stream;
	unsigned char mo_data[LSCHC_IPv6NextHeader_MO];
	cbor_stream_t mo_stream;
	unsigned char cdf_data[LSCHC_IPv6NextHeader_CDF];
	cbor_stream_t cdf_stream;
}LSCHC_IPv6NextHeader;

typedef struct {
	LSCHC_FieldName field_name;
	char field_position;
	LSCHC_DirectionIndicator direction_indicator;
	unsigned char tv_data[LSCHC_IPv6HopLimit_TV];
	cbor_stream_t tv_stream;
	unsigned char mo_data[LSCHC_IPv6HopLimit_MO];
	cbor_stream_t mo_stream;
	unsigned char cdf_data[LSCHC_IPv6HopLimit_CDF];
	cbor_stream_t cdf_stream;
}LSCHC_IPv6HopLimit;

typedef struct {
	LSCHC_FieldName field_name;
	char field_position;
	LSCHC_DirectionIndicator direction_indicator;
	unsigned char tv_data[LSCHC_IPv6ESprefix_TV];
	cbor_stream_t tv_stream;
	unsigned char mo_data[LSCHC_IPv6ESprefix_MO];
	cbor_stream_t mo_stream;
	unsigned char cdf_data[LSCHC_IPv6ESprefix_CDF];
	cbor_stream_t cdf_stream;
}LSCHC_IPv6ESprefix;

typedef struct {
	LSCHC_FieldName field_name;
	char field_position;
	LSCHC_DirectionIndicator direction_indicator;
	unsigned char tv_data[LSCHC_IPv6ESiid_TV];
	cbor_stream_t tv_stream;
	unsigned char mo_data[LSCHC_IPv6ESiid_MO];
	cbor_stream_t mo_stream;
	unsigned char cdf_data[LSCHC_IPv6ESiid_CDF];
	cbor_stream_t cdf_stream;
}LSCHC_IPv6ESiid;

typedef struct {
	LSCHC_FieldName field_name;
	char field_position;
	LSCHC_DirectionIndicator direction_indicator;
	unsigned char tv_data[LSCHC_IPv6LAprefix_TV];
	cbor_stream_t tv_stream;
	unsigned char mo_data[LSCHC_IPv6LAprefix_MO];
	cbor_stream_t mo_stream;
	unsigned char cdf_data[LSCHC_IPv6LAprefix_CDF];
	cbor_stream_t cdf_stream;
}LSCHC_IPv6LCprefix;

typedef struct {
	LSCHC_FieldName field_name;
	char field_position;
	LSCHC_DirectionIndicator direction_indicator;
	unsigned char tv_data[LSCHC_IPv6LAiid_TV];
	cbor_stream_t tv_stream;
	unsigned char mo_data[LSCHC_IPv6LAiid_MO];
	cbor_stream_t mo_stream;
	unsigned char cdf_data[LSCHC_IPv6LAiid_CDF];
	cbor_stream_t cdf_stream;
}LSCHC_IPv6LAiid;

typedef struct {
	LSCHC_IPv6Version 		IPv6Version_field;
	LSCHC_IPv6DiffServ 		IPv6DiffServ_field;
	LSCHC_IPv6FlowLabel	    IPv6FlowLabel_field;
	LSCHC_IPv6Length 		IPv6Length_field;
	LSCHC_IPv6NextHeader 	IPv6NextHeader_field;
	LSCHC_IPv6HopLimit 		IPv6HopLimit_field;
	LSCHC_IPv6ESprefix 		IPv6ESprefix_field;
	LSCHC_IPv6ESiid 		IPv6ESiid_field;
	LSCHC_IPv6LCprefix 		IPv6LCprefix_field;
	LSCHC_IPv6LAiid 		IPv6LAiid_field;
}LSCHC_NLC_IP_Rule;

//////////////////////////////////////////////////
///////////        UDP            ////////////////
typedef struct {
	LSCHC_FieldName field_name;
	char field_position;
	LSCHC_DirectionIndicator direction_indicator;
	unsigned char tv_data[LSCHC_UDPESport_TV];
	cbor_stream_t tv_stream;
	unsigned char mo_data[LSCHC_UDPESport_MO];
	cbor_stream_t mo_stream;
	unsigned char cdf_data[LSCHC_UDPESport_CDF];
	cbor_stream_t cdf_stream;
}LSCHC_UDPESport;

typedef struct {
	LSCHC_FieldName field_name;
	char field_position;
	LSCHC_DirectionIndicator direction_indicator;
	unsigned char tv_data[LSCHC_UDPLAport_TV];
	cbor_stream_t tv_stream;
	unsigned char mo_data[LSCHC_UDPLAport_MO];
	cbor_stream_t mo_stream;
	unsigned char cdf_data[LSCHC_UDPLAport_CDF];
	cbor_stream_t cdf_stream;
}LSCHC_UDPLAport;

typedef struct {
	LSCHC_FieldName field_name;
	char field_position;
	LSCHC_DirectionIndicator direction_indicator;
	unsigned char tv_data[LSCHC_UDPLength_TV];
	cbor_stream_t tv_stream;
	unsigned char mo_data[LSCHC_UDPLength_MO];
	cbor_stream_t mo_stream;
	unsigned char cdf_data[LSCHC_UDPLength_CDF];
	cbor_stream_t cdf_stream;
}LSCHC_UDPLength;

typedef struct {
	LSCHC_FieldName field_name;
	char field_position;
	LSCHC_DirectionIndicator direction_indicator;
	unsigned char tv_data[LSCHC_UDPChecksum_TV];
	cbor_stream_t tv_stream;
	unsigned char mo_data[LSCHC_UDPChecksum_MO];
	cbor_stream_t mo_stream;
	unsigned char cdf_data[LSCHC_UDPChecksum_CDF];
	cbor_stream_t cdf_stream;
}LSCHC_UDPChecksum;

typedef struct {
	LSCHC_UDPESport 		UDPESport_field;
	LSCHC_UDPLAport 		UDPLAport_field;
	LSCHC_UDPLength 		UDPLength_field;
	LSCHC_UDPChecksum 		UDPChecksum_field;
}LSCHC_TLC_UDP_Rule;
//////////////////////////////////////////////////
//////////         ICMPv6        /////////////////



//////////////////////////////////////////////////
///////////        CoAP         //////////////////


//////////////////////////////////////////////////
#endif /* LSCHC_h_ */

